import React, { useState, useEffect } from "react";
import { Container } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import product from "../products.json";

const AboutPage = () => {
  const navigate = useNavigate();
  const [listCart, setListCart] = useState<{id: string, name: string, price: number, quantity: number, stock:number}[]>([]);
  useEffect(() => {
    // Update the document title using the browser API
    console.log(listCart);
  }, [listCart]);



  const hanldeClick = (id: string, name: string, price: number, quantity: number, stock:number) => {
    const data = {id, name, price, quantity};
    if(stock >= quantity){
      setListCart([...listCart, ...[data]]);
    }


  };

  return (
    <Container maxW="container.xl">
      <div>Cart</div>
      <div>
        {
          listCart.map((x, y) =>(
            <div key={y}>
              <div>
              {x.name}
              </div>
              <div>
              {x.price}
              </div>
            </div>
          ))
        }

      </div>


      <div>Product</div>
      {product.map((item, index) => (
        <div key={index}>
          <img src={item.imageUrl} />
          <div>{item.name}</div>
          <div>Rp. {item.price}</div>

          <button onClick={() => hanldeClick(item.id, item.name, item.price, 1, item.quantity)}>
            add to chart
          </button>
        </div>
      ))}
    </Container>
  );
};

export default AboutPage;
